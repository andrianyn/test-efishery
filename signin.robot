*** Settings ***
Library                 SeleniumLibrary

*** Variables ***
${BROWSER}              Chrome
${URL}                  https://music.youtube.com/

# Element Path
${SIGNIN_BTN}           xpath:/html/body/ytmusic-app/ytmusic-app-layout/ytmusic-nav-bar/div[3]/a
${NEXT_BTN_EMAIL}       id:identifierNext
${NEXT_BTN_PASS}        id:passwordNext
${EMAIL_INVALID}        gmail000000000@gmail.com
${EMAIL_VALID}          and.winds8@gmail.com 


*** Test Cases ***
Sign In Features
   TC_1001 Sign In without input Email
   TC_1002 Sign In with invalid Email
   TC_1003 Sign In without input Password


*** Keywords ***
TC_1001 Sign In without input Email
   Open Browser   ${URL}   ${BROWSER}  #options=add_argument("--incognito")
   Maximize Browser Window
   Sleep    3s
   Click Element  ${SIGNIN_BTN}
   Sleep    3s
   Click Element  ${NEXT_BTN_EMAIL}
   Sleep    3s
   Page Should Contain  Enter an email or phone number
   Sleep    3s

TC_1002 Sign In with invalid Email
   Input Text  id=identifierId   ${EMAIL_INVALID}
   Sleep    3s
   Click Element  ${NEXT_BTN_EMAIL}
   Sleep    3s
   Page Should Contain  Couldn’t find your Google Account
   Sleep    3s    

TC_1003 Sign In without input Password
   Clear Element Text  id=identifierId
   Sleep    3s
   Input Text  id=identifierId   ${EMAIL_VALID}
   Sleep    3s
   Click Element  ${NEXT_BTN_EMAIL}
   Sleep    3s
   Page Should Contain  Couldn’t sign you in
   Sleep    3s
   Close Browser 
