*** Settings ***
Library                 SeleniumLibrary

*** Variables ***
${BROWSER}              Chrome
${URL}                  https://music.youtube.com/

# Element Path
${SEARCH_BTN}           id:placeholder
${HOME_BTN}             xpath://*[@id="left-content"]/a/picture/img
${SEARCH_FIELD}         xpath:/html/body/ytmusic-app/ytmusic-app-layout/ytmusic-nav-bar/div[2]/ytmusic-search-box/div/div[1]/input
${KEYWORD_INVALID}      --------
${KEYWORD_ARTIST}       John Mayer
${KEYWORD_SONG}         Slow Dancing in a Burning Room


*** Test Cases ***
Search Features
    TC_2001 Search with Invalid Keyword
    TC_2002 Search with Valid Artist Keyword
    TC_2003 Search with Valid Song Keyword


*** Keywords ***
TC_2001 Search with Invalid Keyword
    Open Browser   ${URL}   ${BROWSER}  options=add_argument("--incognito")
    Maximize Browser Window
    Sleep   3s
    Click Element   ${SEARCH_BTN}
    Sleep   3s
    Input Text   ${SEARCH_FIELD}    ${KEYWORD_INVALID}
    Press Keys  None    RETURN
    Sleep   3s
    Page Should Contain     No results found
    Sleep   3s

TC_2002 Search with Valid Artist Keyword
    Click Element   ${HOME_BTN}
    Sleep   3s
    Click Element   ${SEARCH_BTN}
    Sleep   3s
    Input Text   ${SEARCH_FIELD}    ${KEYWORD_ARTIST}
    Press Keys  None    RETURN
    Sleep   3s
    Page Should Contain     John Mayer
    Sleep   3s  

TC_2003 Search with Valid Song Keyword
    Click Element   ${HOME_BTN}
    Sleep   3s
    Click Element   ${SEARCH_BTN}
    Sleep   3s
    Input Text   ${SEARCH_FIELD}    ${KEYWORD_SONG}
    Press Keys  None    RETURN
    Sleep   3s
    Page Should Contain     Slow Dancing in a Burning Room
    Sleep   3s
    Close Browser