Test Efishey
==================

--------
This test project using Robot Framework. Robot Framework is a generic open source automation framework for acceptance testing, acceptance test driven development (ATDD). It has simple plain text syntax and it can be extended easily with libraries implemented using Python or Java.

--------


What you need
-----------

- The core framework is implemented using Python, supports both Python 2.7 and Python 3.5+, and runs also on Jython (JVM), IronPython (.NET) and PyPy.
- For more information about Robot Framework and the ecosystem, see http://robotframework.org.


Installation
-------------
If you already have Python with pip installed, you can simply run:

    pip install robotframework

Alternatively you can get Robot Framework source code by downloading the source distribution from PyPI and extracting it, or by cloning the project repository from GitHub. After that you can install the framework with:

    python setup.py install


Usage
-------------
	Enter project directory
	$ Robot search.robot
    $ Robot signin.robot


